defmodule StreamChat.MixProject do
  use Mix.Project

  def project do
    [
      app: :stream_chat,
      name: "Stream Chat",
      source_url: "https://bitbucket.org/agencyfusion/stream-chat-elixir",
      homepage_url: "https://bitbucket.org/agencyfusion/stream-chat-elixir",
      version: "0.1.0",
      elixir: "~> 1.9",
      description: description(),
      package: package(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.22", only: :dev, runtime: false},
      {:hackney, "~> 1.16.0"},
      {:httpoison, "~> 1.6"},
      {:jason, "~> 1.1"},
      {:joken, "~> 2.0"}
    ]
  end

  defp description do
    """
    A GetStream Chat client for the Elixir language.
    """
  end

  defp package do
    [
      name: :stream_chat,
      maintainers: ["Daniel Rasband"],
      licenses: ["MIT"],
      links: %{
        "BitBucket" => "https://bitbucket.org/agencyfusion/stream-chat-elixir",
        "Docs" => "https://agencyfusion.bitbucket.io/stream-chat-elixir"
      }
    ]
  end

  defp docs do
    [
      source_url: "https://bitbucket.org/agencyfusion/stream-chat-elixir",
      name: "Stream Chat for Elixir",
      source_url_pattern:
        "https://bitbucket.org/agencyfusion/stream-chat-elixir/src/master/%{path}#lines-%{line}"
    ]
  end
end
