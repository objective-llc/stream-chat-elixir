defmodule StreamChat.Channel do
  @moduledoc "Implements GetStream channel functionality."

  alias StreamChat.{Client, Event}
  alias __MODULE__

  defstruct type: nil, id: nil

  @type t() :: %Channel{}

  @spec url(t()) :: String.t()
  def url(%Channel{id: nil, type: type}) when type not in [nil, ""], do: "channels/#{type}"
  def url(%Channel{id: nil}), do: "channels"
  def url(channel), do: "channels/#{channel.type}/#{channel.id}"

  @spec send_message(t(), map(), String.t()) :: Client.api_response_t()
  def send_message(channel, message, user_id) do
    payload = %{"message" => add_user_id(message, user_id)}

    Client.api_post("#{url(channel)}/message", data: payload)
  end

  @spec send_event(t(), Event.t(), String.t()) :: Client.api_response_t()
  def send_event(channel, event, user_id) do
    payload = %{"event" => add_user_id(event, user_id)}
    Client.api_post("#{url(channel)}/event", data: payload)
  end

  @doc "Create a channel."
  @spec create(String.t(), map(), Keyword.t()) :: Client.api_response_t()
  def create(type, custom_data \\ %{}, opts \\ []) do
    query(
      %Channel{type: type, id: Keyword.get(opts, :id)},
      %{watch: false, state: false, presence: false, custom_data: custom_data},
      opts
    )
  end

  @spec query(t(), map(), Keyword.t()) :: Client.api_response_t()
  def query(channel, params, opts \\ []) do
    custom_data = Map.get(params, :custom_data, %{})
    payload = Map.merge(%{state: true, data: custom_data}, params)
    opts = Keyword.merge([data: payload], opts)

    Client.api_post("#{url(channel)}/query", opts)
  end

  @spec query(map()) :: Client.api_response_t()
  def query(params) do
    channel_type = Map.get(params, :channel_type)
    query(%Channel{type: channel_type}, params)
  end

  @doc "Update a channel."
  @spec update(t(), map(), Keyword.t()) :: Client.api_response_t()
  def update(channel, data, opts \\ []) do
    Client.api_post(url(channel), Keyword.merge(opts, data: data))
  end

  @doc "Delete a channel."
  @spec delete(t()) :: Client.api_response_t()
  def delete(channel), do: Client.api_delete(url(channel))

  @spec truncate(t()) :: Client.api_response_t()
  def truncate(channel), do: Client.api_post("#{url(channel)}/truncate", data: %{})

  @doc "Adds members to the channel; `user_id` is the user responsible for doing this."
  @spec add_members(t(), [String.t()], String.t(), Keyword.t()) :: Client.api_response_t()
  def add_members(channel, user_ids, user_id, opts \\ []) do
    channel
    |> url()
    |> post_with_possible_message(%{"add_members" => user_ids, "user_id" => user_id}, opts)
  end

  @spec invite_members(t(), [String.t()]) :: Client.api_response_t()
  def invite_members(channel, user_ids),
    do: Client.api_post(url(channel), data: %{"invites" => user_ids})

  @doc "Add moderators to a channel."
  @spec add_moderators(t(), [String.t()]) :: Client.api_response_t()
  def add_moderators(channel, user_ids),
    do: Client.api_post(url(channel), data: %{"add_moderators" => user_ids})

  @doc "Remove members from a channel; `user_id` is the user responsible for doing this."
  @spec remove_members(t(), [String.t()], String.t(), Keyword.t()) :: Client.api_response_t()
  def remove_members(channel, user_ids, user_id, opts \\ []) do
    channel
    |> url()
    |> post_with_possible_message(%{"remove_members" => user_ids, "user_id" => user_id}, opts)
  end

  @doc "Demote moderators from a channel."
  @spec demote_moderators(t(), [String.t()]) :: Client.api_response_t()
  def demote_moderators(channel, user_ids),
    do: Client.api_post(url(channel), data: %{"demote_moderators" => user_ids})

  @doc "Mark all of a channel's messages as read for a user."
  @spec mark_read(t(), String.t(), map()) :: Client.api_response_t()
  def mark_read(channel, user_id, params) do
    payload = add_user_id(params, user_id)
    Client.api_post("#{url(channel)}/read", data: payload)
  end

  @doc "Ban a user from a channel."
  @spec ban_user(t(), String.t(), map()) :: Client.api_response_t()
  def ban_user(channel, user_id, params),
    do: Client.ban_user(user_id, %{type: channel.type, id: channel.id, params: params})

  @doc "Unban a user from a channel."
  @spec unban_user(t(), String.t()) :: Client.api_response_t()
  def unban_user(channel, user_id),
    do: Client.unban_user(user_id, %{type: channel.type, id: channel.id})

  @doc "Hide a user in a channel."
  @spec hide(t(), String.t(), Keyword.t()) :: Client.api_response_t()
  def hide(channel, user_id, opts \\ []),
    do: Client.post("#{url(channel)}/hide", Keyword.merge(opts, data: %{user_id: user_id}))

  @doc "Show a user in a channel."
  @spec show(t(), String.t(), Keyword.t()) :: Client.api_response_t()
  def show(channel, user_id, opts \\ []),
    do: Client.api_post("#{url(channel)}/show", Keyword.merge(opts, data: %{user_id: user_id}))

  @doc "Upload a file to a channel."
  @spec send_file(t(), String.t(), map()) :: Client.api_response_t()
  def send_file(channel, file_url, user),
    do: Client.api_send_file("#{url(channel)}/file", file_url, user)

  @doc "Upload an image to a channel."
  @spec send_image(t(), String.t(), map()) :: Client.api_response_t()
  def send_image(channel, file_url, user),
    do: Client.api_send_file("#{url(channel)}/image", file_url, user)

  @spec delete_file(t(), String.t()) :: Client.api_response_t()
  def delete_file(channel, file_url),
    do: Client.api_delete("#{url(channel)}/file", params: %{"url" => file_url})

  @spec delete_image(t(), String.t()) :: Client.api_response_t()
  def delete_image(channel, file_url),
    do: Client.api_delete("#{url(channel)}/image", params: %{"url" => file_url})

  defp add_user_id(%{} = payload, user_id), do: Map.merge(payload, %{user: %{id: user_id}})

  # Utility that will add a message to the data params if present in options. Forwards to post.
  defp post_with_possible_message(url, data, opts) do
    case Keyword.get(opts, :message) do
      %{} = msg -> Client.api_post(url, Keyword.merge(opts, data: Map.put(data, "message", msg)))
      _ -> Client.api_post(url, Keyword.merge(opts, data: data))
    end
  end
end
