defmodule StreamChat.Config do
  @moduledoc "A central location to retrieve configuration."

  alias Joken.Signer

  @default_base_url "https://chat-us-east-1.stream-io-api.com"
  @default_timeout 500

  @doc "Retrieve the API's base URL."
  @spec base_url() :: String.t()
  def base_url, do: Application.get_env(:stream_chat, :base_url) || @default_base_url

  @doc "Retrieve the API key."
  @spec api_key() :: String.t()
  def api_key, do: Application.get_env(:stream_chat, :api_key)

  @doc "Retrieve the API secret."
  @spec api_secret() :: String.t()
  def api_secret, do: Application.get_env(:stream_chat, :api_secret)

  @doc "Retrieve the timeout."
  @spec timeout() :: integer()
  def timeout,
    do: String.to_integer(Application.get_env(:stream_chat, :timeout) || "#{@default_timeout}")

  @doc "A Signer for encoding JWT tokens."
  @spec signer() :: Signer.t()
  def signer, do: Signer.create("HS256", api_secret())
end
