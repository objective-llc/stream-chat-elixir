defmodule StreamChat.User do
  @moduledoc "The user schema."

  alias __MODULE__

  defstruct [:id, :role]

  @type t() :: %User{}
end
