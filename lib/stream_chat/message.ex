defmodule StreamChat.Message do
  @moduledoc "A struct for messages."

  alias __MODULE__
  alias StreamChat.Client

  @type t() :: %Message{}

  defstruct ~w[
    attachments html id mentioned_users parent_id show_in_channel text user user_id
    additional_fields
  ]a

  @spec url(t()) :: String.t()
  def url(%Message{id: nil}), do: "messages"
  def url(message), do: "messages/#{message.id}"

  @spec get(t() | String.t()) :: Client.api_response_t()
  def get(id_or_message, opts \\ [])
  def get(id, opts) when is_binary(id), do: get(%Message{id: id}, opts)
  def get(%Message{} = message, opts), do: Client.api_get(url(message), opts)

  @doc "Get a message's replies."
  @spec get_replies(t() | String.t(), map()) :: Client.api_response_t()
  def get_replies(parent_id, params) when is_binary(parent_id),
    do: get_replies(%Message{id: parent_id}, params)

  def get_replies(%Message{} = parent_message, params),
    do: Client.api_get("messages/#{url(parent_message)}/replies", params: params)

  @doc "Get reactions for a message."
  @spec get_reactions(t() | String.t(), map()) :: Client.api_response_t()
  def get_reactions(message_id, params) when is_binary(message_id),
    do: get_reactions(%Message{id: message_id}, params)

  def get_reactions(%Message{} = message, params),
    do: Client.api_get("#{url(message)}/reactions", params: params)
end

defimpl Jason.Encoder, for: StreamChat.Message do
  alias Jason.Encode
  alias StreamChat.Message

  @json_fields ~w[attachments html id mentioned_users parent_id show_in_channel text user_id]a

  @doc """
  Instead of using @derive Jason.Encoder, add a custom encoder that converts additional fields
  into top-level fields.
  """
  @spec encode(Message.t(), Encode.opts()) :: iodata()
  def encode(%Message{} = value, opts) do
    value
    |> Map.take(@json_fields)
    |> Map.merge(Map.get(value, :additional_fields, %{}))
    |> Encode.map(opts)
  end
end
