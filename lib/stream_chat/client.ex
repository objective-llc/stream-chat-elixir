defmodule StreamChat.Client do
  @moduledoc "Client for the GetStream Chat "

  alias StreamChat.{Config, Token}

  use HTTPoison.Base

  @expected_response_fields ~w[
    access_token app automod automod_behavior channel channel_types channels cid client_id code
    commands connect_events connection_id created_at device_errors devices duration error event
    exception_fields file flag general_errors max_message_length me members message
    message_retention messages mutes name own_user permissions reaction reactions read read_events
    rendered_apn_template rendered_firebase_template replies search type typing_events updated_at
    uploads url_enrichment user user_id users watcher_count watchers
  ]

  @request_options ~w[
    async follow_redirect max_body_length max_redirect params proxy proxy_auth recv_timeout
    socks5_pass socks5_user ssl stream_to timeout
  ]a

  @request_options [:response_type | @request_options]

  @type inner_error_t() :: HTTPoison.Error.t() | Joken.Signer.t()
  @type api_response_t() ::
          {:ok, HTTPoison.Response.t()} | {:error, inner_error_t()} | map() | Keyword.t()

  @impl true
  def process_request_url(relative_url), do: Enum.join([Config.base_url(), relative_url], "/")

  @impl true
  def process_request_options(options) do
    [
      timeout: Config.timeout(),
      recv_timeout: Config.timeout(),
      hackney: [pool: :default]
    ]
    |> Keyword.merge(options)
  end

  @impl true
  def process_request_headers(headers) do
    default_headers()
    |> Enum.into(%{})
    |> Map.merge(Enum.into(headers, %{}))
    |> Enum.into([])
  end

  @impl true
  def process_response_body(body) do
    body
    |> Jason.decode!()
    |> Map.take(@expected_response_fields)
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
  rescue
    Jason.DecodeError -> {:error, body}
    error -> error
  end

  @doc "Create a JWT token for use on the client side."
  @spec create_token(String.t(), integer() | nil) :: binary()
  def create_token(user_id, exp \\ nil) do
    payload = %{"user_id" => user_id}
    payload = if(is_nil(exp), do: payload, else: Map.put(payload, "exp", exp))

    Token.generate_and_sign!(payload, Config.signer())
  end

  @doc "Update your application's settings."
  @spec update_app_settings(map()) :: api_response_t()
  def update_app_settings(%{} = settings), do: patch("/app", settings)

  @doc "Get your application's settings."
  @spec get_app_settings() :: api_response_t()
  def get_app_settings do
    api_get("app")
  end

  @spec flag_message(String.t(), map()) :: api_response_t()
  def flag_message(id, params \\ %{}) do
    payload = Map.merge(%{"target_message_id" => id}, params)

    api_post("moderation/flag", data: payload)
  end

  @spec unflag_message(String.t(), map()) :: api_response_t()
  def unflag_message(id, params) do
    payload = Map.merge(%{"target_message_id" => id}, params)
    api_post("moderation/unflag", data: payload)
  end

  @spec flag_user(String.t(), map()) :: api_response_t()
  def flag_user(id, params) do
    payload = Map.merge(%{"target_user_id" => id}, params)
    api_post("moderation/flag", data: payload)
  end

  @spec unflag_user(String.t(), map()) :: api_response_t()
  def unflag_user(id, params) do
    payload = Map.merge(%{"target_user_id" => id}, params)
    api_post("moderation/unflag", data: payload)
  end

  @spec search(map(), map(), map()) :: api_response_t()
  def search(filter_conditions, query, params) do
    params =
      Map.merge(params, %{
        "filter_conditions" => filter_conditions,
        "query" => query
      })

    api_get("search", params: %{"payload" => Jason.encode!(params)})
  end

  @spec update_users([map()]) :: api_response_t()
  def update_users(users) do
    payload =
      Enum.reduce(users, %{}, fn user, acc ->
        case Map.get(user, :id, Map.get(user, "id")) do
          nil -> raise("user must have an id")
          id -> Map.put(acc, id, user)
        end
      end)

    api_post("users", data: %{"users" => payload})
  end

  @spec update_user(map()) :: api_response_t()
  def update_user(user), do: update_users([user])

  @spec update_users_partial([map()]) :: api_response_t()
  def update_users_partial(updates), do: api_patch("users", data: %{"users" => updates})

  @spec update_user_partial(map()) :: api_response_t()
  def update_user_partial(update), do: update_users_partial([update])

  @spec delete_user(String.t(), map()) :: api_response_t()
  def delete_user(user_id, params), do: api_delete("users/#{user_id}", params: params)

  @spec deactivate_user(String.t(), map()) :: api_response_t()
  def deactivate_user(user_id, params),
    do: api_post("users/#{user_id}/deactivate", params: params)

  @spec reactivate_user(String.t(), map()) :: api_response_t()
  def reactivate_user(user_id, params),
    do: api_post("users/#{user_id}/reactivate", params: params)

  @spec export_user(String.t(), map() | nil) :: api_response_t()
  def export_user(user_id, params), do: api_get("users/#{user_id}/export", params: params)

  @spec ban_user(String.t(), map() | nil) :: api_response_t()
  def ban_user(target_id, data) do
    payload = Map.merge(%{"target_user_id" => target_id}, data)
    api_post("moderation/ban", data: payload)
  end

  @spec unban_user(String.t(), map() | nil) :: api_response_t()
  def unban_user(target_id, params) do
    params = Map.merge(%{"target_user_id" => target_id}, params)
    api_delete("moderation/ban", params: params)
  end

  @spec mute_user(String.t(), String.t()) :: api_response_t()
  def mute_user(target_id, user_id) do
    payload = %{"target_id" => target_id, "user_id" => user_id}
    api_post("moderation/mute", data: payload)
  end

  @spec unmute_user(String.t(), String.t()) :: api_response_t()
  def unmute_user(target_id, user_id) do
    payload = %{"target_id" => target_id, "user_id" => user_id}
    api_post("moderation/unmute", data: payload)
  end

  @spec mark_all_read(String.t()) :: api_response_t()
  def mark_all_read(user_id) do
    payload = %{"user" => %{"id" => user_id}}
    api_post("channels/read", data: payload)
  end

  @spec update_message(map(), Keyword.t()) :: api_response_t()
  def update_message(message, opts \\ []) do
    unless Map.has_key?(message, "id") do
      raise("message must have an id")
    end

    api_post("messages/#{message["id"]}", Keyword.merge(opts, data: %{"message" => message}))
  end

  @spec delete_message(String.t()) :: api_response_t()
  def delete_message(message_id), do: api_delete("messages/#{message_id}")

  @spec query_users(map(), map(), map() | keyword() | [{String.t(), any()}] | nil) ::
          api_response_t()
  def query_users(%{} = filter_conditions, %{} = params, sort \\ nil) do
    sort_fields = build_sort_fields(sort)

    params =
      Map.merge(params, %{
        "filter_conditions" => filter_conditions,
        "sort" => sort_fields
      })

    api_get("users", params: %{"payload" => Jason.encode!(params)})
  end

  @spec query_channels(map() | nil, map() | nil, list() | nil) :: api_response_t()
  def query_channels(filter_conditions, params, sort \\ nil) do
    sort_fields = build_sort_fields(sort)

    params =
      %{
        "state" => true,
        "watch" => false,
        "presence" => false,
        "filter_conditions" => filter_conditions,
        "sort" => sort_fields
      }
      |> Map.merge(params)

    api_get("channels", params: %{"payload" => Jason.encode!(params)})
  end

  @spec create_channel_type(map()) :: api_response_t()
  def create_channel_type(data) do
    data =
      if !Map.has_key?(data, "commands") || Map.get(data, "commands", "") == "" do
        Map.put(data, "commands", "all")
      else
        data
      end

    api_post("channeltypes", data: data)
  end

  @spec get_channel_type(String.t()) :: api_response_t()
  def get_channel_type(channel_type), do: api_get("channeltypes/#{channel_type}")

  @spec list_channel_types() :: api_response_t()
  def list_channel_types, do: api_get("channeltypes")

  @spec update_channel_type(String.t(), map() | nil) :: api_response_t()
  def update_channel_type(channel_type, params),
    do: api_put("channeltypes/#{channel_type}", data: params)

  @spec delete_channel_type(String.t()) :: api_response_t()
  def delete_channel_type(channel_type), do: api_delete("channeltypes/#{channel_type}")

  @spec add_device(String.t(), String.t(), String.t()) :: api_response_t()
  def add_device(device_id, push_provider, user_id) do
    api_post("devices",
      data: %{
        id: device_id,
        push_provider: push_provider,
        user_id: user_id
      }
    )
  end

  @spec delete_device(String.t(), String.t()) :: api_response_t()
  def delete_device(device_id, user_id),
    do: api_delete("devices", params: %{id: device_id, user_id: user_id})

  @spec get_devices(String.t()) :: api_response_t()
  def get_devices(user_id), do: api_get("devices", params: %{user_id: user_id})

  @spec verify_webhook(String.t(), String.t()) :: boolean()
  def verify_webhook(request_body, x_signature) do
    :sha256
    |> :crypto.hmac(Config.api_secret(), request_body)
    |> Base.encode16(case: :lower)
    |> Kernel.==(x_signature)
  end

  @spec api_put(String.t(), keyword()) :: api_response_t()
  def api_put(relative_url, opts \\ []) do
    make_http_request(:put, relative_url, opts)
  end

  @spec api_post(String.t(), keyword()) :: api_response_t()
  def api_post(relative_url, opts \\ []) do
    make_http_request(:post, relative_url, opts)
  end

  @spec api_get(String.t(), keyword()) :: api_response_t()
  def api_get(relative_url, opts \\ []) do
    make_http_request(:get, relative_url, opts)
  end

  @spec api_delete(String.t(), keyword()) :: api_response_t()
  def api_delete(relative_url, opts \\ []) do
    make_http_request(:delete, relative_url, opts)
  end

  @spec api_patch(String.t(), keyword()) :: api_response_t()
  def api_patch(relative_url, opts \\ []) do
    make_http_request(:patch, relative_url, opts)
  end

  @doc "Send a file to any file-upload-enabled API endpoint."
  @spec api_send_file(String.t(), String.t(), map()) :: api_response_t()
  def api_send_file(relative_url, file_path, user) do
    filename = Path.basename(file_path)

    multipart =
      {:multipart,
       [
         {:file, file_path, {"form-data", [name: filename, filename: filename]}},
         {"user", Jason.encode!(user)},
         {"api_key", Config.api_key()}
       ]}

    post(relative_url, multipart)
  end

  defp make_http_request(method, url, opts) do
    url = url_with_query_params(url, opts)
    headers = Keyword.get(opts, :headers, [])
    options = build_request_options(opts)
    body = build_body(opts)

    case {request(method, url, body, headers, options), Keyword.get(opts, :response_type)} do
      {{:ok, %{body: body}}, :map} -> Enum.into(body, %{})
      {{:ok, %{body: body}}, _} -> body
      {{:error, _} = error_response, _} -> error_response
    end
  rescue
    e in Joken.Signer -> {:error, e}
  end

  defp url_with_query_params(url, opts) do
    query =
      opts
      |> Keyword.get(:params, %{})
      |> (&Map.merge(default_params(), &1)).()
      |> URI.encode_query()

    "#{url}?#{query}"
  end

  defp build_request_options(opts) do
    Enum.reduce(opts, [], fn {key, value}, acc ->
      if key in @request_options do
        Keyword.put(acc, key, value)
      else
        acc
      end
    end)
  end

  defp build_body(opts) do
    opts
    |> Keyword.get(:data, nil)
    |> Jason.encode!()
  end

  defp build_sort_fields(nil), do: []

  defp build_sort_fields(sort) do
    Enum.reduce(sort, [], fn {k, v}, acc ->
      [%{"field" => k, "direction" => v} | acc]
    end)
  end

  defp user_agent, do: "stream-chat-elixir-client-#{app_version()}"

  defp app_version do
    {:ok, vsn} = :application.get_key(:stream_chat, :vsn)

    List.to_string(vsn)
  end

  # All API requests need to include the API key.
  defp default_params, do: %{api_key: Config.api_key()}

  # Generate a bearer token for use in all API requests.
  defp auth_token do
    Token.generate_and_sign!(%{"server" => true}, Config.signer())
  end

  defp default_headers do
    [
      {"Content-Type", "application/json"},
      {"X-Stream-Client", user_agent()},
      {"Authorization", auth_token()},
      {"stream-auth-type", "jwt"}
    ]
  end
end
