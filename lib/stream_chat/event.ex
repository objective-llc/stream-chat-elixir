defmodule StreamChat.Event do
  @moduledoc "A schema for channel events."

  alias __MODULE__

  @type t() :: %Event{}

  defstruct ~w[type user user_id additional_fields]a
end

defimpl Jason.Encoder, for: StreamChat.Event do
  alias Jason.Encode
  alias StreamChat.Event

  @doc """
  Instead of using @derive Jason.Encoder, add a custom encoder that converts additional fields
  into top-level fields.
  """
  @spec encode(Event.t(), Encode.opts()) :: iodata()
  def encode(value, opts) do
    value
    |> Map.take(~w[type user_id]a)
    |> Map.merge(Map.get(value, :additional_fields, %{}))
    |> Encode.map(opts)
  end
end
