defmodule StreamChat.Reaction do
  @moduledoc "The reaction struct, with sending and deleting."

  alias __MODULE__
  alias StreamChat.Client

  @type t() :: %Reaction{}

  defstruct [:type, :message_id, :user_id, :user, :additional_fields]

  @doc "Builds a URL for sending/deleting a reaction."
  @spec url(t()) :: String.t()
  def url(reaction), do: "messages/#{reaction.message_id}/reaction"

  @doc "React to a message."
  @spec send(t()) :: Client.api_response_t()
  def send(reaction) do
    payload = %{"reaction" => reaction}
    Client.api_post(url(reaction), data: payload)
  end

  @doc "Delete a user's reaction from a message."
  @spec delete(t()) :: Client.api_response_t()
  def delete(reaction) do
    Client.api_delete("#{url(reaction)}/#{reaction.type}",
      params: %{"user_id" => reaction.user_id}
    )
  end
end

defimpl Jason.Encoder, for: StreamChat.Reaction do
  alias Jason.Encode
  alias StreamChat.Reaction

  @doc """
  Instead of using @derive Jason.Encoder, add a custom encoder that converts additional fields
  into top-level fields.
  """
  @spec encode(Reaction.t(), Encode.opts()) :: iodata()
  def encode(%Reaction{} = value, opts) do
    value
    |> Map.take([:type, :message_id, :user_id])
    |> Map.merge(Map.get(value, :additional_fields, %{}))
    |> Encode.map(opts)
  end
end
